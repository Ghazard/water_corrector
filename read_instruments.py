#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 14:54:07 2021

@author: Antti Laitinen
         antti.laitinen@fmi.fi
"""

# Luetaan picarron data
import numpy as np
import pandas as pd
import os
import re
from pandas.errors import ParserError

from read_db_file import read_db_file

def read_picarro(path, starttime=None, endtime=None, valve_data=False):
    
    # Datojen ja valve-tiedostojen sijainnit

    files = os.listdir(path)

    if('1minavg' in files):
        datafiles = f'{path}/1minavg'
        valve = f'{path}/valve'

        # Luetaan kaikki data-tiedostot
        datas = pd.DataFrame()
        for file in os.listdir(datafiles):
            if(starttime):
                if(pd.to_datetime(file[-14:-4]) < starttime):
                    continue
            elif(endtime):
                if((pd.to_datetime(file[-14:-4]) > endtime)):
                    continue

            #datas = datas.append(pd.read_csv(f'{datafiles}/{file}', sep=',', error_bad_lines=False, header=0))
            #datas = datas.append(pd.read_csv(f'{datafiles}/{file}', sep=',', on_bad_lines='skip', header=0))
            datas = pd.concat([datas, pd.read_csv(f'{datafiles}/{file}', sep=',', on_bad_lines='skip', header=0)])

        datas.loc[:, 'TIMESTAMP'] = pd.to_datetime(datas[datas.columns[0]])
        datas = datas[['TIMESTAMP', *datas.columns[1:-1]]]


        if(datas['port'].unique()[0] == -1):
            valve_data = True

        # Jos halutaan myös valvet, luetaan nekin
        if(valve_data):
            valves = pd.DataFrame()
            for file in os.listdir(valve):
                if (re.match('cfkads\d+_\d+-\d+-\d+.csv', file)):

                    if(starttime):
                        if(pd.to_datetime(file[-14:-4]) < starttime):
                            continue
                        elif(endtime ):
                            if((pd.to_datetime(file[-14:-4]) > endtime)):
                                continue
                    #valves = valves.append(pd.read_csv(f'{valve}/{file}', sep=',', on_bad_lines='skip', header=0))
                    valves = pd.concat([valves, pd.read_csv(f'{valve}/{file}', sep=',', on_bad_lines='skip', header=0)])

            def convert_dates(datecol):
                try:
                    date = pd.to_datetime(datecol)
                except:
                    date = np.nan
                return date

            valves.loc[:, valves.columns[0]] = valves[valves.columns[0]].apply(lambda x: convert_dates(x))
            valves.dropna(subset=[valves.columns[0]], inplace=True)

            datas = datas.merge(valves, left_on=datas.columns[0], right_on=valves.columns[0], how='left')
            datas.dropna(subset=['portID_#0'], inplace=True)
            datas.loc[:, 'port'] = datas['portID_#0'].astype(int)
    else:
        dfs = []
        for root, dirs, files in os.walk(path):
            for file in files:
                dfs.append(pd.read_csv(f'{root}/{file}', delim_whitespace=True))
        datas = pd.concat(dfs)
        print(datas.columns)
        datas = datas.dropna(subset='DATE')

        datas['TIMESTAMP'] = pd.to_datetime(datas.apply(lambda x: f'{x["DATE"]} {x["TIME"]}', axis=1))
        datas = datas[['TIMESTAMP', *datas.columns[8:-1]]]


    #datas = datas.sort_values(by='Minute_start(file+0H)')
    datas = datas.sort_values(by='TIMESTAMP')
    if(starttime and endtime):
        return datas[(datas[datas.columns[0]] > starttime) & ((datas[datas.columns[0]] < endtime))]
    elif(starttime):
        return datas[(datas[datas.columns[0]] > starttime)]
    elif(endtime):
        return datas[((datas[datas.columns[0]] < endtime))]
    else:
        return datas


def read_ftir(path, starttime=None, endtime=None):
    db_name = [x for x in os.listdir(f'{path}') if '.db' in x][0]
    df = read_db_file(f'{path}/{db_name}')
    df['Timestamp'] = pd.to_datetime(df['Collection_Start_Time'])
    if starttime and endtime:
        return df[(df['Timestamp'] > starttime) & (df['Timestamp'] < endtime)]
    elif starttime:
        return df[df['Timestamp'] > starttime]
    elif endtime:
        return df[df['Timestamp'] < endtime]
    else:
        return df

if __name__ == '__main__':
    
    path = '/home/laitinea/Documents/iCOS/Vertailut/data/G5310'
    path_ftri = '/home/laitinea/Documents/iCOS/Vertailut/data/FTIR/PAL.db'
    path = '/home/laitinea/Documents/iCOS/Auditoinnit/Reunion/data/2405'
    #path = '/home/laitinea/Documents/iCOS/Auditoinnit/Svartberget/data/2401'
    #path = '/media/laitinea/KINGSTON/PUI/Vestitesti'
    
    starttime = pd.to_datetime('01.03.2021', dayfirst=True)
    endtime = pd.to_datetime('01.04.2021 23:00', dayfirst=True)
    
    #starttime = pd.to_datetime('08.11.2021 14:30', dayfirst=True)
    #endtime = pd.to_datetime('09.11.2021 05:40', dayfirst=True)

    df = read_picarro(path, starttime=None, endtime=None)
    print(df.columns)
    #df = read_ftir(path_ftri)