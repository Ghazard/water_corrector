# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 15:37:11 2020

@author: laitinea
"""

"""
Script to calculate the water correction for picarro
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
import seaborn as sns
import configparser
import os
from matplotlib.ticker import AutoMinorLocator
from scipy.optimize import curve_fit

from functions import find_plateaus, odr_fit


def water_correction(datafile, starttime, endtime):
    # Read the picarro ID
    pid = datafile.split('/')[-1].split('_')[0][-4:]

    # Check if folder for the instrument exists, if not make one
    basepath = '/'.join(datafile.split('/')[:-1])
    if (not os.path.isdir(f'{basepath}/{pid}')):
        os.mkdir(f'{basepath}/{pid}')

    # Read the data
    filetype = datafile.split('.')[-1]
    if (filetype == 'xlsx'):
        df = pd.read_excel(datafile)

        # Read the coefficients from the second sheet
        df2 = pd.read_excel(datafile, sheet_name=2).iloc[:, [1, 2]].dropna()
        coefs = {}
        coefs[df2.columns[0]] = df2.columns[1]
        for i, row in df2.iterrows():
            coefs[row[0].strip()] = row[1]

    elif (filetype == 'csv'):
        df = pd.read_csv(datafile)

        # Read the coefficient from .init file
        initfile = '/'.join(datafile.split('/')[0:-1]) + f'/InstrCal_CFKADS_{pid}.ini'
        config = configparser.ConfigParser()
        config.read(initfile)
        coefs = config._sections['Data']
        for coef, value in coefs.items():
            coefs[coef] = float(value)

    elif (filetype == 'dat'):
        df = pd.read_csv(datafile, delim_whitespace=True)
        df['Minute_start(file+0H)'] = df.apply(lambda x: pd.to_datetime(x['DATE'] + ' ' + x['TIME']), axis=1)

    else:
        print(f'Unknown filetype: {filetype}')
        return 0

    # Convert time to datetime
    df['Minute_start(file+0H)'] = pd.to_datetime(df['Minute_start(file+0H)'])
    date = df['Minute_start(file+0H)'].iloc[0].strftime('%d-%m-%y')

    # Take only the water calibration period
    (starth, startm) = starttime.split(':')
    (endh, endm) = endtime.split(':')

    starttime = pd.to_datetime(df['Minute_start(file+0H)'][0].replace(hour=int(starth), minute=int(startm)))
    endtime = pd.to_datetime(df['Minute_start(file+0H)'][0].replace(hour=int(endh), minute=int(endm)))
    df = df[(df['Minute_start(file+0H)'] > starttime) & (df['Minute_start(file+0H)'] < endtime)]

    # Try to remove the peaks in water
    for column in ['avg(h2o_reported)', 'avg(b_h2o_pct)']:
        peaks = find_peaks(df[column], threshold=0.1)[0]
        df.drop(df.index[peaks], axis=0, inplace=True)

    # Find the plateaus for water
    plateaus = find_plateaus(df['avg(h2o_reported)'])

    # Drop the 5 first minutes and 2 last of each plateau
    for start, end in plateaus:
        first_mins = df['Minute_start(file+0H)'].iloc[start:start + 5]
        last_mins = df['Minute_start(file+0H)'].iloc[end - 2:end]
        df = df[(~df['Minute_start(file+0H)'].isin(first_mins)) & (~df['Minute_start(file+0H)'].isin(last_mins))]

    # Find the plateaus again
    plateaus = find_plateaus(df['avg(h2o_reported)'])

    h2o = [np.mean(df['avg(h2o_reported)'].iloc[start:end]) for (start, end) in plateaus]
    h2oerr = [np.std(df['avg(h2o_reported)'].iloc[start:end]) for (start, end) in plateaus]

    h2o.append(np.mean(df['avg(h2o_reported)'].iloc[5:plateaus[0, 0] - 4]))
    h2oerr.append(np.std(df['avg(h2o_reported)'].iloc[5:plateaus[0, 0] - 4]))

    # First do the fit for CO2 and CH4
    for i, column in enumerate(['avg(CO2)', 'avg(CH4)']):
        fig, ax = plt.subplots()
        normalized = df[column] / df[column].iloc[[0, -1]].mean()

        # Take the gas data
        gas = [np.mean(normalized.iloc[start:end]) for (start, end) in plateaus]
        gaserr = [np.std(normalized.iloc[start:end]) for (start, end) in plateaus]

        # And add the 0-water data
        gas.append(np.mean(normalized.iloc[5:plateaus[0, 0] - 4]))
        gaserr.append(np.std(normalized.iloc[5:plateaus[0, 0] - 4]))

        fit = np.polyfit(df['avg(h2o_reported)'], normalized, deg=2)

        fit = np.polyfit(h2o, gas, deg=2)

        # Using scipy and pass through 1
        def fit_func(x, a, b):
            # Curve fitting function
            return a * x ** 2 + b * x + 1  #

        ax.errorbar(h2o, gas, xerr=h2oerr, yerr=gaserr, marker='.', linestyle='', capsize=1, color='k')

        fit = odr_fit(h2o, gas, h2oerr, gaserr, degree=2, force_int=1)
        x = np.linspace(min(df['avg(h2o_reported)']), max(df['avg(h2o_reported)']), num=1000)
        y = fit[0] * x ** 2 + fit[1] * x + 1

        ax.plot(x, y, 'k-')

        ax.text(np.quantile(x, 0.25), max(y), f'$f(x)$ = {fit[0]:.6f}$x^2$ + {fit[1]:.6f}$x$ + 1')

        sns.despine()
        ax.set_xlabel('H2O %')
        ax.set_ylabel(column[4:7] + ' normal')

        ax.set_title(f'Picarro {pid}')

        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        fig.savefig(f'{basepath}/{pid}/water_correction_{column[4:7]}_{date}.png', dpi=300)

    # Then for CO which is more complicated
    CO2Peak14lnPpm = df['avg(peak_14)'] * coefs['co2_conc_slope']
    CoPeakWithOffset = df['avg(peak84_raw)'] + coefs['co_offset']

    CORaw = CoPeakWithOffset + coefs['co_water_linear'] * df['avg(b_h2o_pct)'] \
            + coefs['co_water_quadratic'] * df['avg(b_h2o_pct)'] ** 2 \
            + coefs['co_water_co2'] * CO2Peak14lnPpm * df['avg(b_h2o_pct)'] \
            + coefs['co_co2_linear'] * CO2Peak14lnPpm

    COWet = CORaw * coefs['co_conc_slope']
    df['CO_dry'] = COWet / (1 + coefs['co_wd_linear'] * df['avg(h2o_reported)'] + coefs['co_wd_quadratic'] * df[
        'avg(h2o_reported)'] ** 2)

    # H2O correction bias
    H2OCorrBias = df['avg(CO)'] - df['avg(CO)'].iloc[0]

    # Calculate Delta CO
    dCO = coefs['co_offset'] + coefs['co_water_linear'] * df['avg(b_h2o_pct)'] \
          + coefs['co_water_quadratic'] * df['avg(b_h2o_pct)'] ** 2 \
          + coefs['co_co2_linear'] * (coefs['co2_conc_slope'] * df['avg(peak_14)'] + coefs['co2_conc_intercept']) \
          + coefs['co_water_co2'] * (coefs['co2_conc_slope'] * df['avg(peak_14)'] + coefs['co2_conc_intercept']) \
          * df['avg(b_h2o_pct)']

    CO_dry_ref = np.mean(df['avg(CO)'].iloc[plateaus[0][0]:plateaus[0][1]][5:])
    A = coefs['co_conc_intercept']
    B = coefs['co_conc_slope']
    C = coefs['co_offset']
    D = coefs['co_water_linear']
    E = coefs['co_water_quadratic']
    F = coefs['co_water_co2']
    G = coefs['co2_conc_slope']
    H = coefs['co2_conc_intercept']
    I = coefs['co_co2_linear']
    J = coefs['co_wd_linear']
    K = coefs['co_wd_quadratic']
    deltaCO = (((CO_dry_ref - A)/B)*(1 + J*df['avg(h2o_reported)'] + K*df['avg(h2o_reported)']**2)) - \
              (df['avg(peak84_raw'] + C + F*df['avg(b_h2o_pct)']*(G*df['avg(peak_14)'] + H) +
               I*(G*df['avg(peak_14)'] + H))

    # Take the means and stds from plateaus
    deltaCO_mean = [np.mean(deltaCO[start:end]) for (start, end) in plateaus]
    deltaCO_err = [np.std(deltaCO[start:end]) for (start, end) in plateaus]

    deltaCO_mean.append(np.mean(deltaCO.iloc[5:plateaus[0, 0] - 4]))
    deltaCO_err.append(np.std(deltaCO.iloc[5:plateaus[0, 0] - 4]))

    h2o_b = [np.mean(df['avg(b_h2o_pct)'].iloc[start:end]) for (start, end) in plateaus]
    h2oerr_b = [np.std(df['avg(b_h2o_pct)'].iloc[start:end]) for (start, end) in plateaus]

    h2o_b.append(np.mean(df['avg(b_h2o_pct)'].iloc[5:plateaus[0, 0] - 4]))
    h2oerr_b.append(np.std(df['avg(b_h2o_pct)'].iloc[5:plateaus[0, 0] - 4]))

    fit = np.polyfit(df['avg(b_h2o_pct)'], dCO, deg=2)

    # Take the means and stds from plateaus 
    #dCO_mean = [np.mean(dCO[start:end]) for (start, end) in plateaus]
    #dCO_err = [np.std(dCO[start:end]) for (start, end) in plateaus]

    #dCO_mean.append(np.mean(dCO.iloc[5:plateaus[0, 0] - 4]))
    #dCO_err.append(np.std(dCO.iloc[5:plateaus[0, 0] - 4]))

    #fit = np.polyfit(df['avg(b_h2o_pct)'], dCO, deg=2)

    def fit_func(x, a, b):
        # Curve fitting function
        return a * x ** 2 + b * x  # d=0 is implied

    fit = curve_fit(fit_func, h2o, deltaCO_mean)[0]
    fit_ODR = odr_fit(h2o, deltaCO_mean, h2oerr, deltaCO_err, degree=2)

    #fit = curve_fit(fit_func, h2o, dCO_mean)[0]
    #fit_ODR = odr_fit(h2o, dCO_mean, h2oerr, dCO_err, degree=2, force_int=0)

    print(f'Laitteelle {pid}:\nh2o:{h2o}\ndCO:{deltaCO_mean}\nh2oerr:{h2oerr}\ndCOerr:{deltaCO_err}')

    fig, ax = plt.subplots()
    ax.errorbar(h2o, deltaCO_mean, xerr=h2oerr, yerr=deltaCO_err, marker='.', linestyle='')

    #print(f'Laitteelle {pid}:\nh2o:{h2o}\ndCO:{dCO_mean}\nh2oerr:{h2oerr}\ndCOerr:{dCO_err}')

    #fig, ax = plt.subplots()
    #ax.errorbar(h2o, dCO_mean, xerr=h2oerr, yerr=dCO_err, marker='.', linestyle='')

    x = np.linspace(min(df['avg(h2o_reported)']), max(df['avg(h2o_reported)']), num=1000)
    y = fit_ODR[0] * x ** 2 + fit_ODR[1] * x

    ax.plot(x, y, 'k-', label='ODR')
    fig.legend()

    ax.text(np.quantile(x, 0.25), max(y), f'$f(x)$ = {fit[0]:.6f}$x^2$ + {fit[1]:.6f}$x$ + 0')

    sns.despine()
    ax.set_xlabel('H2O %')
    ax.set_ylabel('$\Delta$CO normal')
    ax.set_title(f'Picarro {pid}')

    plt.tight_layout()

    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    fig.savefig(f'{basepath}/{pid}/water_correction_CO_{date}.png', dpi=300)

    # Plot the water and the h2o correction bias
    h2oavg = np.array([])
    h2oerr = np.array([])
    corravg = np.array([])
    correrr = np.array([])
    co2rravg = np.array([])
    co2rrerr = np.array([])
    for start, end in plateaus:
        h2oavg = np.append(h2oavg, df['avg(h2o_reported)'].iloc[start:end].mean())
        h2oerr = np.append(h2oerr, df['avg(h2o_reported)'].iloc[start:end].std())
        corravg = np.append(corravg, np.mean(H2OCorrBias[start:end]))
        correrr = np.append(correrr, np.std(H2OCorrBias[start:end]))
        co2rravg = np.append(co2rravg, np.mean(df['avg(CO2)'] - df['avg(CO2_dry)']))
        co2rrerr = np.append(co2rrerr, np.std(df['avg(CO2)'] - df['avg(CO2_dry)']))

    fig, ax = plt.subplots()

    # Multiply by 1000 to get ppb
    ax.errorbar(h2oavg, corravg * 1000, xerr=h2oerr, yerr=correrr * 1000, fmt='k.', capsize=4, linewidth=1)
    ax.axhline(0, color='red')
    sns.despine()
    ax.set_xlabel('H2O reported [%]')
    ax.set_ylabel('CO correction bias [ppb]')
    ax.set_title(f'Picarro {pid}')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    fig.savefig(f'{basepath}/{pid}/water_correction_bias_CO_{date}.png', dpi=300)

    fig, ax = plt.subplots()

    # Multiply by 1000 to get ppb
    ax.errorbar(h2oavg, corravg * 1000, xerr=h2oerr, yerr=correrr * 1000, fmt='k.', capsize=4, linewidth=1)
    ax.axhline(0, color='red')
    sns.despine()
    ax.set_xlabel('H2O reported [%]')
    ax.set_ylabel('CO2 correction bias [ppb]')
    ax.set_title(f'Picarro {pid}')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    fig.savefig(f'{basepath}/{pid}/water_correction_bias_CO2_{date}.png', dpi=300)

    # Assign the newly determined values for CO_water_linear and CO_water_quadratic
    coefs['CO_water_linear_new'] = fit_ODR[1]
    coefs['CO_water_quadratic_new'] = fit_ODR[0]

    return df, coefs


if __name__ == '__main__':
    # Close all the previous plots
    plt.close('all')

    dpath = '../Vesikalibroinnit/cfkads2187_2018-08-28_WB.xlsx'
    dpath = '../Vesikalibroinnit/cfkads2187_2018-10-22_WB.xlsx'
    dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Pallas_03_2021/Sekalaista/cfkads2187_2021-04-19.csv'
    dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Pallas_03_2021/Sekalaista/cfkads2187_2021-04-19.csv'
    # dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Puijo_09_2021/Data/PUI/2394/CFKADS2394-20210921-232431Z-DataLog_User.dat'

    # df2 = pd.read_excel(dpath,sheet_name=2)

    starttime = '14:00'
    endtime = '19:30'
    df, coefs = water_correction(dpath, starttime, endtime)

    # df.plot('Minute_start(file+0H)', 'avg(h2o_reported)')

    dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Pallas_03_2021/Sekalaista/cfkads2164_1minavg_2021-04-19.csv'
    # dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Puijo_09_2021/Data/PUI/2394/CFKADS2394-20210921-232431Z-DataLog_User.dat'

    # df = water_correction(dpath, starttime, endtime)[0]

    # fig,ax = plt.subplots()
    # ax.set_title('avg H2O')
    # df.plot('Minute_start(file+0H)', 'avg(h2o_reported)', ax=ax)
    # fig,ax = plt.subplots()
    # ax.set_title('avg CO')
    # df.plot('Minute_start(file+0H)', 'avg(CO)', ax=ax)
    # fig,ax = plt.subplots()
    # ax.set_title('CO corr bias')
    # ax.plot(df['Minute_start(file+0H)'], df['avg(CO)'] - df['avg(CO)'].iloc[0])

    # peaks = find_peaks(df['avg(h2o_reported)'],threshold=0.1)[0]
    # df.drop(df.index[peaks],axis=0,inplace=True)
    # peaks = find_peaks(df['avg(h2o_reported)'],threshold=0.1)[0]
    # df.drop(df.index[peaks],axis=0,inplace=True)

    # for column in df.columns[1:]:
    #     df.plot(x='Minute_start(file+0H)',y=column)
