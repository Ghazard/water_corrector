#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 11:22:40 2020

@author: laitinea
"""

import numpy as np
from scipy.signal import find_peaks
from scipy.odr import Model, Data, ODR, RealData
from scipy.optimize import curve_fit

def find_plateaus(signal, mean_coefficient=0.7):


    signal = np.array(signal)
    # Get the differences from the next data point
    diffs = np.abs(np.diff(signal, prepend=signal[0]))
    
    # Find the median and the std of the differences
    diffmed = np.median(diffs)
    diffstd = np.std(diffs)
    diffmean = np.mean(diffs)
    
    # Take the peaks to be those that are more than 2 std from the median
    thold = diffmed + 1*diffstd
    thold = diffmean * mean_coefficient
    
    # Peaks in diff
    peaks = find_peaks(diffs, threshold=thold)[0]
    
    plateaus = []
    for i, ind in enumerate(peaks):
        if i == 0:
            continue
        plateau = np.array([peaks[i-1]+1, ind-1])
        #plateaus = np.append([plateaus],[plateau],axis=0)
        plateaus.append(plateau)

    return np.array(plateaus)


def odr_fit(x, y, xerr, yerr, degree, force_int=None):
    
    if(degree == 1 and (force_int is not None)):
        def fit_func(x, a):
            return a*x + force_int
        def fit_func_ODR(B, x):
            return B[0]*x + force_int
    elif(degree == 1):
        def fit_func(x, a, b):
            return a*x + b
        def fit_func_ODR(B, x):
            return B[0]*x + B[1]
    elif(degree == 2 and (force_int is not None)):
        def fit_func(x, a, b):
            return a*x**2 + b*x + force_int
        def fit_func_ODR(B, x):
            return B[0]*x**2 + B[1]*x + force_int
    elif(degree == 2):
        def fit_func(x, a, b, c):
            return a*x**2 + b*x + c
        def fit_func_ODR(B, x):
            return B[0]*x**2 + B[1]*x + B[2]
        
    else:
        print('Degree must be 1 or 2!')
        return
    
    pol = Model(fit_func_ODR)
    mydata = RealData(x, y, xerr, yerr)
    
    # Initialize curve fit parameters
    beta0 = curve_fit(fit_func, x, y)[0]
    
    myodr = ODR(mydata, pol, beta0=beta0)
    
    myoutput = myodr.run()
    
    return myoutput.beta


if __name__ == '__main__':
    
    import pandas as pd
    import matplotlib.pyplot as plt
    import numpy as np
    
    plt.close('all')
    
    #dpath = '../Vesikalibroinnit/cfkads2187_2018-10-22_WB.xlsx'
    dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Pallas_03_2021/Sekalaista/cfkads2164_1minavg_2021-03-02.csv'
    
    starttime = '15:00'
    endtime = '20:00'
    
    
    #df = pd.read_excel(dpath)
    df = pd.read_csv(dpath)
    
    # Convert time to datetime
    df['Minute_start(file+0H)'] = pd.to_datetime(df['Minute_start(file+0H)'])
 
    # Take only the water calibration period
    (starth, startm) = starttime.split(':')
    (endh, endm) = endtime.split(':')

    starttime = pd.to_datetime(df['Minute_start(file+0H)'][0].replace(hour=int(starth), minute=int(startm)))
    endtime = pd.to_datetime(df['Minute_start(file+0H)'][0].replace(hour=int(endh), minute=int(endm)))
    df = df[(df['Minute_start(file+0H)'] > starttime) & (df['Minute_start(file+0H)'] < endtime)]

    for column in ['avg(h2o_reported)', 'avg(b_h2o_pct)']:
        peaks = find_peaks(df[column], threshold=0.1)[0]
        df.drop(df.index[peaks], axis=0, inplace=True)
        
    plateaus = find_plateaus(df['avg(h2o_reported)'])
    
    fig, ax = plt.subplots()
    df.plot(x='Minute_start(file+0H)',y='avg(h2o_reported)',ax=ax)
    
    for plateau in plateaus:
        df.iloc[plateau[0]:plateau[1]].plot(x='Minute_start(file+0H)',y='avg(h2o_reported)',ax=ax,color='red',label='_nolabel_')

    plt.show()
        
    # Testataan ODR_fit funktio
    x = np.random.rand(10)
    y = x + (np.random.rand(10) / 10)
    
    xerr = np.random.rand(10) / 1000
    yerr = np.random.rand(10) / 1000
    
    def fit_func(x, a, b):
        return a*x + b
    
    output = odr_fit(x, y, xerr, yerr, degree=1)
    
    fit = x*output[0] + output[1]
    
    plt.close('all')
    
    fig, ax = plt.subplots()
    ax.plot(x,y, '.')
    ax.plot(x,fit, '-', color='red')
    plt.show()
    