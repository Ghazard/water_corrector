# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from read_instruments import read_picarro
from WaterCorrector import WaterCorrector
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    path = '/home/laitinea/Documents/iCOS/Auditoinnit/Reunion/data/2405'
    path = '/home/laitinea/Documents/iCOS/Auditoinnit/Svartberget/data/2401'
    pid = path.split('/')[-1]
    df = read_picarro(path)
    instcal = '/'.join(path.split('/')[:-1]) + f'/InstrCal/InstrCal_CFKADS_{pid}.ini'

    starttime = pd.to_datetime('09.05.2023 17:30', dayfirst=True)
    endtime = pd.to_datetime('10.05.2023 03:00', dayfirst=True)

    df = df[(df['TIMESTAMP'] > starttime) & (df['TIMESTAMP'] < endtime)]
    df = df.resample('1min', on='TIMESTAMP').mean().reset_index()


    df.plot(x='TIMESTAMP', y=['avg(CO2)'])
    df.plot(x='TIMESTAMP', y=['avg(h2o_reported)'])

    corrector = WaterCorrector()
    corrector.read_constants(instcal)
    corrector.calculate_coefficients(df, averaged=False, mean_coefficient=0.7)

    corrector.print_corrections()
    corrector.plot_correction()

    corrected_df = corrector.correct_data(df, suffix='_MobileLab')

    Mlab_co2 = [-2.623e-04, -1.182e-02]
    Mlab_ch4 = [-2.013e-04, -9.623e-03]
    Mlab_co = [-2.165e-03, -1.634e-03, 3.997e-03]
    corrector_Mlab = WaterCorrector()
    corrector_Mlab.read_constants(instcal)

    corrector_Mlab.set_coefficient([Mlab_co2, Mlab_ch4, Mlab_co])
    corrected_df = corrector_Mlab.correct_data(df, suffix='_Mlab')


    sorted_df = corrected_df.sort_values(by='h2o_reported')
    for gas in ['CO2', 'CH4', 'CO']:
        corrected_df[f'{gas}_WSCorr_diff'] = corrected_df[f'{gas}_MobileLab'] - corrected_df[f'{gas}_Mlab']

    sorted_df = corrected_df.sort_values(by='h2o_reported')

    print('\n')
    for gas in ['CO2', 'CH4', 'CO']:
        fig, ax = plt.subplots()
        sorted_df.plot(x='h2o_reported', y=f'{gas}_MobileLab', ax=ax, marker='.', linestyle='', markersize=1)
        sorted_df.plot(x='h2o_reported', y=f'{gas}_Mlab', ax=ax, marker='.', linestyle='', markersize=1)

        sns.despine()

        dry_df = sorted_df[sorted_df['h2o_reported'] < 0.003]
        print(f'Dry {gas}: {sorted_df[f"{gas}_dry"].mean():.4f}')

        #hdf = sorted_df[(sorted_df['h2o_reported']>0.450) & (sorted_df['h2o_reported']<0.550)]
        #print(f'For {gas} at {hdf["h2o_reported"].mean():.4f} humidity the diff is {hdf[f"{gas}_WSCorr_diff"].mean():.4f}')
        #print(f'The MLab dry value: {hdf[f"{gas}_Mlab"].mean():.4f}')
        #print(f'The Mobile Lab dry value: {hdf[f"{gas}_MobileLab"].mean():.4f}')

        hdf = sorted_df[(sorted_df['h2o_reported'] > 2.54) & (sorted_df['h2o_reported'] < 2.65)]
        print(f'For {gas} at {hdf["h2o_reported"].mean():.4f} humidity the diff is {hdf[f"{gas}_WSCorr_diff"].mean():.4f}')
        print(f'The MLab dry value: {hdf[f"{gas}_Mlab"].mean():.4f}')
        print(f'The Mobile Lab dry value: {hdf[f"{gas}_MobileLab"].mean():.4f}')
        print(f'The {gas} wet is {hdf[f"{gas}"].mean():.4f}')
        print('\n')

    plt.show()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
