#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 11:28:03 2022

@author: Antti Laitinen
         antti.laitinen@fmi.fi
"""

import pandas as pd
import os
import numpy as np
import gzip


def open_csv(path):
    filetype = path.split('.')[-1]
    if filetype == 'dat':
        return pd.read_csv(path, delim_whitespace=True)
    elif filetype == 'csv':
        return pd.read_csv(path)
    else:
        return pd.DataFrame()


def open_gzip(path):
    filetype = path.split('.')[-2]
    
    with gzip.open(path) as f:
        if filetype == 'dat':
            return pd.read_csv(f, delim_whitespace=True)
        elif filetype == 'csv':
            return pd.read_csv(f)
        else:
            return pd.DataFrame()


#def read_folder(path):
#    for path, folders, files in os.walk(path):
#        
#        for file in files:
#            if(file.split('.')[-1] == 'gz'):
#                with gzip.open(f'{path}/{file}') as f:


def prepare_files(path, starttime, endtime):
    
    # Iterate through the folders 
    datapaths = {}
    instrcal = False
    for i in os.scandir(path):
        if i.is_dir():
            if i.name == 'InstrCal':
                instrcal = True
            else:
                datapaths[i.name] = i.path
    if not instrcal:
        print(f'Instrument calibration folder not found! Make sure to put the instrument calibration files in {path}/InstrCal')
    
    dfs = {}
    for pid, path in datapaths.items():
        df = pd.DataFrame()
        for path, folders, files in os.walk(path):
            
            for file in files:
                
                if file.split('.')[-1] == 'gz':
                    df_read = open_gzip(f'{path}/{file}')
                    if not df_read.empty:
                        df = pd.concat([df, df_read])
                    else:
                        print(f'Error reading file {path}/{file}')
                else:
                    df_read = open_csv(f'{path}/{file}')
                    if not df_read.empty:
                        df = pd.concat([df, df_read])
                    else:
                        print(f'Error reading file {path}/{file}')
            
        df['DATETIME'] = pd.to_datetime(df.apply(lambda x: f'{x["DATE"]}T{x["TIME"]}', axis=1))
        dfs[pid] = df.drop(labels=['DATE', 'TIME'], axis=1)

    return dfs


def prepare_xlsx(path):
    df = pd.read_excel(path)
        
    # Read the coefficients from the second sheet
    df2 = pd.read_excel(path, sheet_name=2).iloc[:, [1, 2]].dropna()
    coefs = {df2.columns[0]: df2.columns[1]}
    for i, row in df2.iterrows():
        coefs[row[0].strip()] = row[1]
        
    return df, coefs


def main():
    
    path = '/home/laitinea/Documents/iCOS/Vesitesti11052022/'
    
    dfs = prepare_files(path, np.nan, np.nan)
    print(dfs['2168'])
    
    return dfs


if __name__ == '__main__':
    dfs = main()