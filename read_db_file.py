# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import sqlite3
import numpy as np
import pandas as pd

# Define function to read the data from .db file and returns them in pandas datafarme

# Default cols to read:
default_cols =  {'sysvariables': ['Collection_Start_Time', 'Collection_End_Time', 'Inlet'], 
           'aiaverages':['Cell_Pressure_Avg', 'Cell_Temperature_Avg', 'Aux_Temperature_Avg', 'Flow_Out_Avg', 
                         'Flow_In_Avg', 'Thermistor_1_Avg', 'Thermistor_2_Avg', 'Thermistor_3_Avg', 
                         'Case_Temperature_Avg'],
           'aistddeviation':['Cell_Pressure_StdDev', 'Cell_Temperature_StdDev', 'Aux_Temperature_StdDev', 'Flow_Out_StdDev', 
                         'Flow_In_StdDev', 'Thermistor_1_StdDev', 'Thermistor_2_StdDev', 'Thermistor_3_StdDev', 
                         'Case_Temperature_StdDev'], 
           'analysisprimary': ['CO2', 'CO2_1', 'CO2_2', 'CH4', 'N2O', 'CO', 'H2O', 'CO2_3'],
           'calcvals': ['CV_del13C', 'CV_del18O']}


def read_db_file(db_path, wanted_cols=default_cols):
    
    # Create a statement from dictionary where the keys are tables and values are list of columns associated with each 
    # table
    def create_SQL_fetch(columns, limit=None):
        statement = 'SELECT'
        for key, values in columns.items():
            for value in values:
                statement = f'{statement} {key}.{value},'
        
        statement = statement[:-1]
        statement = f'{statement}\nFROM {list(columns.keys())[0]}'
        for key in list(columns.keys())[1:]:
            statement = f'{statement}\nJOIN {key} ON sysvariablesPK = {key}ID'
            
        if(limit):
            statement = f'{statement}\nLIMIT {limit}'
            
        #print(statement)
        return statement
        
    SQL = create_SQL_fetch(wanted_cols)
    
    # Create dictionary to save the data
    data = {}
    # And add columns
    for column_list in wanted_cols.values():
        for column in column_list:
            data[column] = []
                
    # Connect to database
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    # Execute the SQL statement
    c.execute(SQL)
    
    for row in c.fetchall():
        for i, element in enumerate(row):
            data[list(data.keys())[i]].append(element)
            
    return pd.DataFrame(data)




if __name__ == '__main__':
    
    # Path to the database file
    db_file = '/home/laitinea/Documents/iCOS/Vertailut/data/FTIR/PAL.db'
    
    # Columns we want
    columns = {'sysvariables': ['Collection_Start_Time', 'Collection_End_Time', 'Inlet'], 
           'aiaverages':['Cell_Pressure_Avg', 'Cell_Temperature_Avg', 'Aux_Temperature_Avg', 'Flow_Out_Avg', 
                         'Flow_In_Avg', 'Thermistor_1_Avg', 'Thermistor_2_Avg', 'Thermistor_3_Avg', 
                         'Case_Temperature_Avg'],
           'aistddeviation':['Cell_Pressure_StdDev', 'Cell_Temperature_StdDev', 'Aux_Temperature_StdDev', 'Flow_Out_StdDev', 
                         'Flow_In_StdDev', 'Thermistor_1_StdDev', 'Thermistor_2_StdDev', 'Thermistor_3_StdDev', 
                         'Case_Temperature_StdDev'], 
           'analysisprimary': ['CO2', 'CO2_1', 'CO2_2', 'CH4', 'N2O', 'CO', 'H2O', 'CO2_3'],
           'calcvals': ['CV_del13C', 'CV_del18O']}
    
    data = read_db_file(db_file)
    