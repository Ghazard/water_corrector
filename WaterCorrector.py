import time

import pandas as pd
import numpy as np
import matplotlib

# matplotlib.use('TkAgg', force=True)
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
import seaborn as sns
import configparser
import os
from matplotlib.ticker import AutoMinorLocator
from scipy.optimize import curve_fit

from functions import find_plateaus, odr_fit

components = {'co2': 'avg(CO2)', 'ch4': 'avg(CH4)', 'co': 'avg(CO)'}


class WaterCorrector:
    def __init__(self):
        self.gases = {}
        self.gases_errs = {}
        self.h2oerr = None
        self.h2o = None
        self.coefs_determined = {}
        self.coefs = {}
        self.components = components
        return

    def read_constants(self, fpath):
        filetype = fpath.split('.')[-1]
        if filetype == 'xlsx':

            df = pd.read_excel(fpath, sheet_name=2).iloc[:, [1, 2]].dropna()
            self.coefs[df.columns[0]] = df.columns[1]
            for i, row in df.iterrows():
                self.coefs[row[0].strip()] = row[1]

        elif filetype == 'ini':

            # Read the coefficient from .init file
            config = configparser.ConfigParser()
            config.read(fpath)
            self.coefs = {k: float(v) for k, v in config['Data'].items()}

        elif filetype == 'dat':
            print('Unable to read coefs from DAT file yet!')
            return 0

        else:
            print(f'Unknown filetype: {filetype}')
            return 0

    def calculate_coefficients(self, df, averaged=True, mean_coefficient=0.7):
        # TODO: Katso mitkä kertoimet on nyt kiinnostavia!

        if('avg(h2o_reported)' in df.columns):
            col_h2o_r = 'avg(h2o_reported)'
            col_h20_b = 'avg(b_h2o_pct)'
            timecol = 'Minute_start(file+0H)'
            col_co2 = 'avg(CO2)'
            col_ch4 = 'avg(CH4)'
            col_CO = 'avg(CO)'
            col_peak14 = 'avg(peak_14)'
            col_peak84 = 'avg(peak84_raw)'
        else:
            col_h2o_r = 'h2o_reported'
            col_h20_b = 'b_h2o_pct'
            timecol = 'TIMESTAMP'
            col_co2 = 'CO2'
            col_ch4 = 'CH4'
            col_CO = 'CO'
            col_peak14 = 'peak_14'
            col_peak84 = 'peak84_raw'

        A = self.coefs['co_conc_intercept']
        B = self.coefs['co_conc_slope']
        C = self.coefs['co_offset']
        D = self.coefs['co_water_linear']
        E = self.coefs['co_water_quadratic']
        F = self.coefs['co_water_co2']
        G = self.coefs['co2_conc_slope']
        H = self.coefs['co2_conc_intercept']
        I = self.coefs['co_co2_linear']
        J = self.coefs['co_wd_linear']
        K = self.coefs['co_wd_quadratic']

        if ('TIMESTAMP' in df.columns):
            timecol = 'TIMESTAMP'

        # Try to remove the peaks in water
        for column in [col_h2o_r, col_h20_b]:
            peaks = find_peaks(df[column], threshold=0.1)[0]
            df.drop(df.index[peaks], axis=0, inplace=True)

        # Find the plateaus for water
        plateaus = find_plateaus(df[col_h2o_r], mean_coefficient=mean_coefficient)

        if __name__ == '__main__':
            print(f'Plateaut on {plateaus}')

            fig, ax = plt.subplots()
            ax.plot(df.reset_index()[col_h2o_r])
            ax.plot(df.reset_index()[col_h20_b])
            to_remove = []


            for plateau in plateaus[:]:
                mean = df[col_h2o_r].iloc[plateau[0] + 5:plateau[1] - 2].mean()
                ax.plot([plateau[0] + 5, plateau[1] - 2], 2 * [mean], color='crimson')

            mean = df[col_h2o_r].iloc[plateaus[0, 0] - 15: plateaus[0, 0] - 4].mean()
            ax.plot([plateaus[0, 0] - 15, plateaus[0, 0] - 4], 2 * [mean], color='crimson')

            mean = df[col_h2o_r].iloc[plateaus[0, 0] - 20: plateaus[0, 0] - 10].mean()
            ax.plot([plateaus[0, 0] - 20, plateaus[0, 0] - 10], 2 * [mean], color='forestgreen')

            mean = df[col_h2o_r].iloc[plateaus[-1, -1] + 15:-5].mean()
            ax.plot([plateaus[-1, -1] + 15, df.shape[0] - 5], 2 * [mean], color='forestgreen')
            ax.set_title('Plateaus')

        """
        # Drop the 5 first minutes and 2 last of each plateau if short plateau, otherwise 10 and 5
        for start, end in plateaus:
            if (end - start) > 20:
                first_mins = df[timecol].iloc[start:end-15]
                last_mins = df[timecol].iloc[end - 5:end]
            elif (end - start) < 10:
                continue
            else:
                first_mins = df[timecol].iloc[start:start + 5]
                last_mins = df[timecol].iloc[end - 2:end]
            df = df[(~df[timecol].isin(first_mins)) & (~df[timecol].isin(last_mins))]

        # Find the plateaus again
        plateaus = find_plateaus(np.array(df[col_h2o_r]))
        print(f'Plateaut on {plateaus}')
        """

        # Drop plateaus that are too short:
        to_remove = []
        # print(f'Before removing short ones: {plateaus}')
        for i, plateau in enumerate(plateaus):
            if (plateau[1] - plateau[0]) < 10:
                to_remove.append(i)
        plateaus = np.delete(plateaus, to_remove, axis=0)
        # print(f'After removing short ones: {plateaus}')

        """
        fig, ax = plt.subplots()
        ax.plot(df.reset_index()[col_h2o_r])
        for plateau in plateaus:
            if (plateau[1] - plateau[0]) > 10:
                mean = df[col_h2o_r].iloc[plateau[0]+5:plateau[1]-2].mean()
                ax.plot(plateau[0]+5, plateau[1]-2, 2*[mean], color='crimson')
        ax.set_title('After dropping plateaus')
        """

        h2o = [np.mean(df[col_h2o_r].iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]
        h2oerr = [np.std(df[col_h2o_r].iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]

        h2o_b = [np.mean(df[col_h20_b].iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]
        h2oerr_b = [np.std(df[col_h20_b].iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]

        if __name__ == '__main__':
            print(h2o)
            print(h2oerr)

        h2o.append(np.mean(df[col_h2o_r].iloc[5:plateaus[0, 0] - 4]))
        h2oerr.append(np.std(df[col_h2o_r].iloc[5:plateaus[0, 0] - 4]))

        h2o_b.append(np.mean(df[col_h20_b].iloc[5:plateaus[0, 0] - 4]))
        h2oerr_b.append(np.std(df[col_h20_b].iloc[5:plateaus[0, 0] - 4]))

        self.h2o = np.array(h2o)
        self.h2oerr = np.array(h2oerr)

        self.h20_b = np.array(h2o_b)
        self.h2oerr_b = np.array(h2oerr_b)

        def fit_func(x, a, b):
            # Curve fitting function
            return a * x ** 2 + b * x + 1  #

        # First do the fit for CO2 and CH4
        for component, column in zip(['co2', 'ch4'], [col_co2, col_ch4]):
            normalized = df[column] / df[column].iloc[[0, -1]].mean()
            normalized = df[column] / df[column].iloc[[0, -1]].mean()
            dry_component = np.array(df[column].iloc[plateaus[0, 0] - 20:plateaus[0, 0] - 10])
            dry_component = np.append(dry_component, df[column].iloc[plateaus[-1, -1] + 15:-5])
            normalized = df[column] / dry_component.mean()

            # Take the gas data
            gas = [np.mean(normalized.iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]
            gaserr = [np.std(normalized.iloc[start + 5:end - 2]) for (start, end) in plateaus[:-1]]

            # And add the 0-water data
            gas.append(np.mean(normalized.iloc[5:plateaus[0, 0] - 4]))
            gaserr.append(np.std(normalized.iloc[5:plateaus[0, 0] - 4]))

            # fit = np.polyfit(df['avg(h2o_reported)'], normalized, deg=2)
            # fit = np.polyfit(h2o, gas, deg=2)

            # Using scipy and pass through 1
            # def fit_func(x, a, b):
            #    # Curve fitting function
            #    return a * x ** 2 + b * x + 1  #

            fit = odr_fit(h2o, gas, h2oerr, gaserr, degree=2, force_int=1)
            self.coefs_determined[component] = fit
            self.gases[component] = gas
            self.gases_errs[component] = gaserr

        # Then for CO which is more complicated
        CO2Peak14lnPpm = df[col_peak14] * self.coefs['co2_conc_slope']
        CoPeakWithOffset = df[col_peak84] + self.coefs['co_offset']

        CORaw = CoPeakWithOffset + self.coefs['co_water_linear'] * df[col_h20_b] \
                + self.coefs['co_water_quadratic'] * df[col_h20_b] ** 2 \
                + self.coefs['co_water_co2'] * CO2Peak14lnPpm * df[col_h20_b] \
                + self.coefs['co_co2_linear'] * CO2Peak14lnPpm

        COWet = CORaw * self.coefs['co_conc_slope']
        df['CO_dry'] = COWet / (1 + self.coefs['co_wd_linear'] * df[col_h2o_r] +
                                self.coefs['co_wd_quadratic'] * df[col_h2o_r] ** 2)

        # H2O correction bias
        H2OCorrBias = df[col_CO] - df[col_CO].iloc[0]

        # Calculate Delta CO
        dCO = self.coefs['co_offset'] + self.coefs['co_water_linear'] * df[col_h20_b] \
              + self.coefs['co_water_quadratic'] * df[col_h20_b] ** 2 \
              + self.coefs['co_co2_linear'] * (
                      self.coefs['co2_conc_slope'] * df[col_peak14] + self.coefs['co2_conc_intercept']
              ) \
              + self.coefs['co_water_co2'] * (self.coefs['co2_conc_slope'] * df[col_peak14]
                                              + self.coefs['co2_conc_intercept']) * df[col_h20_b]

        CO_dry_ref = np.mean(df[col_CO].iloc[plateaus[0][0]:plateaus[0][1]][5:])
        deltaCO = (((CO_dry_ref - A) / B) * (1 + J * df[col_h2o_r] + K * df[col_h2o_r] ** 2)) - \
                  (df[col_peak84] + C + F * df[col_h20_b] * (G * df[col_peak14] + H) +
                   I * (G * df[col_peak14] + H))

        if __name__ == '__main__':
            fig, ax = plt.subplots()
            ax.plot(df[timecol], deltaCO)
            ax.plot(df[timecol], df[col_peak84])
            ax.plot(df[timecol], df[col_peak14])

        #dCO_mean = [np.mean(dCO[start:end]) for (start, end) in plateaus[:-1]]
        #dCO_err = [np.std(dCO[start:end]) for (start, end) in plateaus[:-1]]

        #dCO_mean.append(np.mean(dCO.iloc[5:plateaus[0, 0] - 4]))
        #dCO_err.append(np.std(dCO.iloc[5:plateaus[0, 0] - 4]))

        dCO_mean = [np.mean(deltaCO[start:end]) for (start, end) in plateaus[:-1]]
        dCO_err = [np.std(deltaCO[start:end]) for (start, end) in plateaus[:-1]]

        dCO_mean.append(np.mean(deltaCO.iloc[5:plateaus[0, 0] - 4]))
        dCO_err.append(np.std(deltaCO.iloc[5:plateaus[0, 0] - 4]))

        fit = np.polyfit(df[col_h2o_r], dCO, deg=2)

        def fit_func(x, a, b):
            # Curve fitting function
            return a * x ** 2 + b * x  # d=0 is implied

        fit = curve_fit(fit_func, h2o, dCO_mean)[0]
        fit_ODR = odr_fit(h2o_b, dCO_mean, h2oerr_b, dCO_err, degree=2)

        self.gases['co'] = dCO_mean
        self.gases_errs['co'] = dCO_err

        self.coefs_determined['co'] = fit_ODR

        ## TODO: Miten palautetaan ja mitä?
        return self.coefs_determined

    def set_coefficient(self, coefs):
        gaseslist = ['co2', 'ch4', 'co', 'n2o']
        for coef, gas in zip(coefs, [gaseslist[i] for i in range(len(coefs))]):
            self.coefs_determined[gas] = coef

    def plot_correction(self):
        ## TODO: Tämä plottaus!

        fig, axs = plt.subplots(nrows=len(self.components), squeeze=False, sharex=True)
        axs.flatten()[-1].set_xlabel('H$_2$O [%]')
        for ax, component in zip(axs.flatten(), self.components.keys()):
            ax.errorbar(self.h2o, self.gases[component], xerr=self.h2oerr, yerr=self.gases_errs[component],
                        marker='.', linestyle='', capsize=1, color='k')

            x = np.linspace(min(self.h2o), max(self.h2o), num=1000)

            if len(self.coefs_determined[component]) == 3:
                y = self.coefs_determined[component][2] + self.coefs_determined[component][1] * x + \
                    self.coefs_determined[component][0] * x ** 2

                eqs = f'$f(x)$ = {self.coefs_determined[component][0]:.6f}$x^2$ + ' \
                      f'{self.coefs_determined[component][1]:.6f}$x$ + {self.coefs_determined[component][2]:.6f}'

            else:
                y = self.coefs_determined[component][0] * x ** 2 + self.coefs_determined[component][1] * x + 1
                eqs = f'$f(x)$ = {self.coefs_determined[component][0]:.6f}$x^2$ + ' \
                      f'{self.coefs_determined[component][1]:.6f}$x$ + 1'

            ax.plot(x, y, 'k-')
            ax.text(np.quantile(x, 0.25), max(y), eqs)
            ax.set_ylabel(component)

        axs.flatten()[0].set_ylabel('CO$_{2, wet}$ / CO$_{2, dry}$')
        axs.flatten()[1].set_ylabel('CH$_{4, wet}$ / CH$_{4, dry}$')
        axs.flatten()[2].set_ylabel('$\Delta$CO')
        sns.despine()
        plt.show()
        return fig

    def print_corrections(self):
        print('CO2:')
        print(f'\tlinear init: {self.coefs["co2_watercorrection_linear"]}')
        print(f'\tlinear determined: {self.coefs_determined["co2"][1]}')
        print(
            f'\tlinear difference: {np.abs(self.coefs["co2_watercorrection_linear"] - self.coefs_determined["co2"][1])}')
        print('\n')
        print(f'\tquadratic init: {self.coefs["co2_watercorrection_quadratic"]}')
        print(f'\tquadratic determined: {self.coefs_determined["co2"][0]}')
        print(
            f'\tquadratic difference: {np.abs(self.coefs["co2_watercorrection_quadratic"] - self.coefs_determined["co"][0])}')

        print('CH4:')
        print(f'\tlinear init: {self.coefs["ch4_watercorrection_linear"]}')
        print(f'\tlinear determined: {self.coefs_determined["ch4"][1]}')
        print(
            f'\tlinear difference: {np.abs(self.coefs["ch4_watercorrection_linear"] - self.coefs_determined["ch4"][1])}')
        print('\n')
        print(f'\tquadratic init: {self.coefs["ch4_watercorrection_quadratic"]}')
        print(f'\tquadratic determined: {self.coefs_determined["ch4"][0]}')
        print(
            f'\tquadratic difference: {np.abs(self.coefs["ch4_watercorrection_quadratic"] - self.coefs_determined["ch4"][0])}')

        print('CO:')
        print(f'\tintercept init: {self.coefs["co_water_linear"]}')
        print(f'\tintercept determined: {self.coefs_determined["co"][2]}')
        print(f'\tintercept difference: {np.abs(self.coefs["co_water_linear"] - self.coefs_determined["co"][2])}')
        print('\n')
        print(f'\tlinear init: {self.coefs["co_water_linear"]}')
        print(f'\tlinear determined: {self.coefs_determined["co"][1]}')
        print(f'\tlinear difference: {np.abs(self.coefs["co_water_linear"] - self.coefs_determined["co"][1])}')
        print('\n')
        print(f'\tquadratic init: {self.coefs["co_water_quadratic"]}')
        print(f'\tquadratic determined: {self.coefs_determined["co"][0]}')
        print(f'\tquadratic difference: {np.abs(self.coefs["co_water_quadratic"] - self.coefs_determined["co"][0])}')

    def correct_data(self, df, suffix='_calculated'):

        if ('avg(h2o_reported)' in df.columns):
            col_h2o_r = 'avg(h2o_reported)'
            col_h20_b = 'avg(b_h2o_pct)'
            timecol = 'Minute_start(file+0H)'
            col_co2 = 'avg(CO2)'
            col_ch4 = 'avg(CH4)'
            col_CO = 'avg(CO)'
            col_peak14 = 'avg(peak_14)'
            col_peak84 = 'avg(peak84_raw)'
        else:
            col_h2o_r = 'h2o_reported'
            col_h20_b = 'b_h2o_pct'
            timecol = 'TIMESTAMP'
            col_co2 = 'CO2'
            col_ch4 = 'CH4'
            col_CO = 'CO'
            col_peak14 = 'peak_14'
            col_peak84 = 'peak84_raw'

        A = self.coefs['co_conc_intercept']
        B = self.coefs['co_conc_slope']
        C = self.coefs['co_offset']
        D = self.coefs_determined['co'][1]
        E = self.coefs_determined['co'][0]
        F = self.coefs['co_water_co2']
        G = self.coefs['co2_conc_slope']
        H = self.coefs['co2_conc_intercept']
        I = self.coefs['co_co2_linear']
        J = self.coefs['co_wd_linear']
        K = self.coefs['co_wd_quadratic']

        # For CO2 and CH4 is rather easy:
        for gas, col in zip(['CO2', 'CH4'], [col_co2, col_ch4]):
            df[f'{gas}{suffix}'] = df[col] / \
                                   (1 +
                                    self.coefs_determined[f"{gas.lower()}"][1] * df[col_h2o_r] +
                                    self.coefs_determined[f"{gas.lower()}"][0] * df[col_h2o_r] ** 2)

        # For CO, a bit more difficult:
        COw = (A + B*(df[col_peak84] + C + D*df[col_h20_b] + E*df[col_h20_b]**2 + F*df[col_h20_b]*(G*df[col_peak14] + H) +
                      I*(G*df[col_peak14] + H)))
        COd = COw / (K*df[col_h2o_r]**2 + J*df[col_h2o_r] + 1)

        df[f'CO{suffix}'] = COd

        return df


if __name__ == '__main__':
    plt.close('all')
    corrector = WaterCorrector()
    ini_file = '/home/laitinea/Documents/iCOS/Vesitesti11052022/InstrCal/InstrCal_CFKADS_2187.ini'
    dpath = '/home/laitinea/Documents/iCOS/Auditoinnit/Pallas_03_2021/Sekalaista/cfkads2187_2021-04-19.csv'

    df = pd.read_csv(dpath)
    df['Minute_start(file+0H)'] = pd.to_datetime(df['Minute_start(file+0H)'])
    df = df[(df['Minute_start(file+0H)'].dt.hour >= 14) & (df['Minute_start(file+0H)'].dt.hour <= 18)]
    corrector.read_constants(ini_file)
    # corrector.read_data(dpath)
    corrector.calculate_coefficients(df)

    df = corrector.correct_data(df)

    corrector.plot_correction()

    print(corrector.coefs)

    fig, ax = plt.subplots()
    df.plot(x='Minute_start(file+0H)', y='avg(CO2_dry)', ax=ax)
    df.plot(x='Minute_start(file+0H)', y='CO2_calculated', ax=ax)
    fig, ax = plt.subplots()
    df['corr_factor'] = df['avg(CO2)'] / df['avg(CO2_dry)']
    df['corr_factor_calc'] = (1 + corrector.coefs_determined[f"co2"][1] * df["avg(h2o_reported)"] +
                              corrector.coefs_determined[f"co2"][0] * df["avg(h2o_reported)"] ** 2)
    df['corr_factor_assign'] = (1 + corrector.coefs["co2_watercorrection_linear"] * df["avg(h2o_reported)"] +
                                corrector.coefs["co2_watercorrection_quadratic"] * df["avg(h2o_reported)"] ** 2)
    df.plot(x='Minute_start(file+0H)', y='corr_factor_assign', ax=ax)
    df.plot(x='Minute_start(file+0H)', y='corr_factor_calc', ax=ax)
    df.plot(x='Minute_start(file+0H)', y='corr_factor', ax=ax)

    fig, ax = plt.subplots()
    df['corr_factor_diff'] = df['corr_factor_calc'] - df['corr_factor']
    df.plot(x='Minute_start(file+0H)', y='corr_factor_diff', ax=ax)

    fig, ax = plt.subplots()
    df['dry_diff'] = df['CO2_calculated'] - df['avg(CO2_dry)']
    df.plot(x='avg(h2o_reported)', y='dry_diff', ax=ax, marker='.', linestyle='')
